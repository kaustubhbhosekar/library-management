'''
Description:-

    Date:-17th March 2020.
    Topic:-Library management System .
    Functionality:-Program to replicate the basic functionality of a Library management system 
    Author:-Kaustubh.S.Bhosekar.

'''
class Library:
	def __init__(self,list, name):
		self.book_list = list
		self.name = name
		self.lend_dict = {}

	def displayBooks(self):
		'''
		Description:- This function displays all the books present in the library
		'''

		print(f"We have following books in our library {self.name}")
		for book in self.book_list:
			print(book)

	def lendBook(self, user, book):
		'''
		Description:- This function lends a pirticular book to a pirticular Student
		'''
		if book not in self.lend_dict.keys():
			self.lend_dict.update({book:user})
			print(f"Lender Book Database has been updated. You can take the book now")
		else:
			print(f"Book is already being used by {self.lend_dict[book]} !")

	def addBook(self, book):
		'''
		Description:- This function Allows the user to add books to the library Collection
		'''
		self.book_list.append(book)
		print(f"Book has been updated to the Book List")

	def returnBook(self, book):
		'''
		Description:- This function Allows the user return a book to the Library.
		'''
		if book in self.lend_dict.keys():
			self.lend_dict.pop(book)
		else:
			print(f"Book is not Issued by ths LIbrary Hence cannot Return.")
	


if __name__ == "__main__":
	'''
	Description:- The Main Function entry point of the Program
	'''
	lib = Library(['Python','Rich Dad Poor Dad','C++ Basics','Algorithms by CLRS'],'Kaustubh Bhosekar Library ')

	while(True):
		print("welcome to {lib.name} Library . enter ypur choice to continue ")
		print("1 Display Books")
		print("2 Lend Book")
		print("3 Add Book")
		print("4 Return Book")
		
		user_choice = input()
		if(user_choice not in ['1','2','3','4']):
			print("Kindly enter a valid Option")
			continue
		else:			
			user_choice = int(user_choice)

		if(user_choice == 1):
			lib.displayBooks()
		elif(user_choice == 2):
			lib.lendBook(input("Enter name of the Person:-"),input("Enter the name of the Book:-"))
		elif(user_choice == 3):
			lib.addBook(input("Enter the name of the book you want to add:-"))
		elif(user_choice == 4):
			lib.returnBook(input("Enter the name of the book you want to return:-"))
		else:
			print("Choose a correct Option form the list.")


		print("Press q to quit or c to continue:-")
		user_choice2 = None
		while(user_choice2 != "q" and user_choice2 != "c" ):
			user_choice2 = input()

			if(user_choice2 == "q"):
				exit()
			if(user_choice2 == "c"):
				continue
